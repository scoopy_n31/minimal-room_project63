<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT ;  

// Login API (for both Admin & User)
$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT UserID, password, IsAdmin FROM shoproom_user WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
	
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "shoproom_system",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			$loginResult->isAdmin = $result[0]->IsAdmin;
			
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
		}
	}
	return response()->json($loginResult);
});

$router->post('/register_user', function(Illuminate\Http\Request $request) {
	
	$name = $request->input("name");
	$surname = $request->input("surname");
	$email = $request->input("email");
	$username = $request->input("username");
	$password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into shoproom_user
					(Username, Password, Name, Surname, Email, IsAdmin)
					VALUE (?, ?, ?, ?, ?, ?)',
					[ $username,
					  $password,
					  $name,
					  $surname,
					  $email,
					  'n'] );
	return "Ok";
	
});


/* admin เพิ่มสินค้า */

$router->post('/admin_add_product', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT IsAdmin FROM shoproom_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		$product_name = $request->input("product_name");	
		$product_category = $request->input("product_category");	
		$product_price = $request->input("product_price");
		$product_rating = $request->input("product_rating");
		$product_numreviews = $request->input("product_numreviews");
		
		$query = app('db')->insert('INSERT into shoproom_product
						(ProductName, Category, Price, Rating, NumReviews)
						VALUE (?, ?, ?, ?, ?)',
						[ $product_name,
						  $product_category,
						  $product_price,
						  $product_rating,
						  $product_numreviews
						  ] );
		return "Ok";
	}
}]);

$router->put ('/admin_update_product',['middleware' => 'auth', function(Illuminate\Http\Request $request){
	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select("SELECT IsAdmin FROM shoproom_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		$reg_id = $request->input("reg_id");
		$query = app('db')->update('UPDATE payment
						SET Confirm=?
						WHERE RegID=?',
						[ 'y',
						  $reg_id] );
		
		return "Ok";	
	}
}]);

$router->get('/admin_list_register', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {
	
	$user = app('auth')->user();
	$user_id = $user->id;
	
	
	$results = app('db')->select("SELECT IsAdmin FROM shoproom_user WHERE UserID=?",
								  [$user_id]);
	
	if ($results[0]->IsAdmin == 'n') {
		return "Permission Denied";
	}else {
		
			$results = app('db')->select('SELECT RegID, Name, Surname, ProductName, Confirm
												FROM , paymeny, shoproom_user
												WHERE (payment.ProductID=shoproom_product.ProductID)
												AND (payment.Confirm=?)
												AND  (shoproom_user.UserID=payment.UserID)',
												['n']);
		
		return response()->json($results);
	}
	
}]);


