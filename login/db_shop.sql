-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for shoproom_system
CREATE DATABASE IF NOT EXISTS `shoproom_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shoproom_system`;

-- Dumping structure for table shoproom_system.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `RegID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) DEFAULT '0',
  `UserID` int(11) DEFAULT '0',
  `Confirm` char(50) DEFAULT 'n',
  PRIMARY KEY (`RegID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table shoproom_system.payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;

-- Dumping structure for table shoproom_system.shoproom_product
CREATE TABLE IF NOT EXISTS `shoproom_product` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(50) NOT NULL,
  `Category` varchar(50) NOT NULL,
  `Price` int(11) NOT NULL,
  `Rating` float NOT NULL,
  `NumReviews` float NOT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table shoproom_system.shoproom_product: ~5 rows (approximately)
/*!40000 ALTER TABLE `shoproom_product` DISABLE KEYS */;
REPLACE INTO `shoproom_product` (`ProductID`, `ProductName`, `Category`, `Price`, `Rating`, `NumReviews`) VALUES
	(1, 'กระจก', 'โคมไฟ', 189, 4.5, 10),
	(2, 'พัดลมพกพา', 'โคมไฟ', 120, 4.5, 10),
	(3, 'กล่องเทป', 'โคมไฟ', 129, 4.5, 10),
	(4, 'กล่องทิชชู่', 'โคมไฟ', 169, 4.5, 10),
	(5, 'กล่อง 5 ช่อง', 'โคมไฟ', 99, 4.5, 10);
/*!40000 ALTER TABLE `shoproom_product` ENABLE KEYS */;

-- Dumping structure for table shoproom_system.shoproom_user
CREATE TABLE IF NOT EXISTS `shoproom_user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `IsAdmin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table shoproom_system.shoproom_user: ~7 rows (approximately)
/*!40000 ALTER TABLE `shoproom_user` DISABLE KEYS */;
REPLACE INTO `shoproom_user` (`UserID`, `Username`, `Password`, `Name`, `Surname`, `Email`, `IsAdmin`) VALUES
	(1, 'scoopy_n31', '$2y$10$3CR/u6nhk0/t5wqMChcNEuGbk08Wbb53nVsVw0LGG2qOZAeuPqd6.', 'Phannita', 'Chuayhlua', '6210210492@psu.ac.th', 'n'),
	(2, 'admin', '$2y$10$xaWUayAO8p4ZYM2KGMHWQ.spYF8WujLWdHUIRvlVzhyqgMAW.UtzK', NULL, NULL, NULL, 'n'),
	(3, NULL, '$2y$10$EOxDwzQVdxFROTSSGgItYuyPLA97BUKigY9sf0Bndp9xox1zqvrBi', NULL, NULL, NULL, 'n'),
	(4, 'admin', '$2y$10$J2PdZ2kVJQzDCbXE5DGaPOPXJXStyaQrfWsD2k3GQhL/6Zm1RyIKm', NULL, NULL, NULL, 'n'),
	(5, NULL, '$2y$10$zDmOrXi3rl9xYAD8bpty8.DzLs9.S3zi1TECCwOcasoi/DXvh0TWa', NULL, NULL, NULL, 'n'),
	(6, NULL, '$2y$10$vgWmo7hdkGKdCOaatYWh0./YQphFN7ubvM9fRKK2lYpLEwZ/o9E1W', NULL, NULL, NULL, 'n'),
	(7, NULL, '$2y$10$HACiglRBPvVPA1yj8pDB/.pNyfj8QyIvpan76MVVdnyXJGZzgqCOi', NULL, NULL, NULL, 'n'),
	(8, NULL, '$2y$10$TqNGbV3ZxVdnnlXsjVLnuOphNhhKcLKcizWAsj8vjDPoLl4tVf/JC', NULL, NULL, NULL, 'n'),
	(9, NULL, '$2y$10$9.MuEA5MCFlQ0TMdTjBGo.p8o/pBibEKbR1I.1Xqon9qSxH0kNSJu', NULL, NULL, NULL, 'n');
/*!40000 ALTER TABLE `shoproom_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
